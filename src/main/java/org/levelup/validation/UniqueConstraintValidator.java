package org.levelup.validation;

import org.hibernate.validator.internal.constraintvalidators.hv.LengthValidator;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueConstraintValidator implements ConstraintValidator<Unique, Object>, ApplicationContextAware{

    private ApplicationContext applicationContext;
    private UniqueValidator uniqueValidator;
    private LengthValidator lengthValidator = new LengthValidator();

    @Override
    public void initialize(Unique constraintAnnotation) {
        Class<? extends UniqueValidator> validator = constraintAnnotation.uniqueValidator();
        if (validator.isInterface()) {
            throw new IllegalArgumentException("UniqueValidator must be a class, not interface");
        }
        uniqueValidator = applicationContext.getBean(validator);
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // lengthValidator.isValid(((String)value).trim(), context);

        return uniqueValidator.validate(value);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
