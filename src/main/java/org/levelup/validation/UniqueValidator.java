package org.levelup.validation;

public interface UniqueValidator {

    boolean validate(Object object);

}
