package org.levelup.validation.dto;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.levelup.validation.Unique;
import org.levelup.validation.UniqueLoginValidator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Unique(uniqueValidator = UniqueLoginValidator.class, message = "Пользователь с таким логином уже существует")
    @NotBlank(message = "Логин не может быть пустым")
    private String login;

    @NotBlank
    @Length(min = 8, max = 15)
    private String password;

}
